# Online Judge Example
This example shows that Sandbox-Run can be used in online judge system.
Sandbox-Run only takes care the compilation and execution of source code.
The part of checking solution is left for implementer.
This is intended to increase the reusablity of Sandbox-Run.

# Setup Environment
1. Install Node.js and Docker
2. Run `npm install`
3. Run `docker pull tomlau10/sandbox-run`

# Start Server
1. Run `node server.js`

# Submission Test
1. Start server
2. Run `node test.js`

# Server Config
- Stored in `config.json`
