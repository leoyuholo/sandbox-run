#!/bin/sh -ex

# cd to script's directory
cd "$(dirname "$BASH_SOURCE")"

# build the sandbox-run image
docker build -t sandbox-run .

# remove untagged images
docker rmi -f $(docker images | awk "/<none>/{print \$3}")
