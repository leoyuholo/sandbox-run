/* include libraries */
#include "common.h"
#include "parse_opts.h"
#include "result_json.h"
#include "compile_code.h"
#include "monitor_prog.h"
#include "io_redirect.h"

/* constants */
#define LOG_FILE	"run.log"
#define NULL_FILE	"/dev/null"

/* main */
int main(int argc, char **argv) {
	// set stdout to line buffered
	if (!isatty(fileno(stdout))) {
		setvbuf(stdout, NULL, _IOLBF, 0);
	}
	
	// parse options from argv
	struct user_config ucfg;
	if (parse_argv(&ucfg, argc, argv) != 0) {
		display_usage(argv[0]);
		exit(1);
	}
	
	// prepare log files
	FILE *log_file = lopen(ucfg.enable_logging, LOG_FILE, "w");
	
	// run program with inputs, even if no inputs it should at least be run once
	int i = 0;
	do {
		// redirect i/o if necessary
		if (ucfg.in != stdin) redirect_to_file(&ucfg.in, "stdin", i, "r");
		if (ucfg.out != stdout) redirect_to_file(&ucfg.out, "stdout", i, "w");
		if (ucfg.err != stderr) redirect_to_file(&ucfg.err, "stderr", i, "w");
		
		// close stdin if no stdin file or no test cases input
		if (!ucfg.in || !ucfg.num_cases) {
			if (ucfg.in) fclose(ucfg.in);
			ucfg.in = fopen(NULL_FILE, "r");
		}
		
		// call compile procedure if it is the first case
		int ret = OK;
		if (!i) {
			lprintf(log_file, "Calling Compile Procedure\n");
			ret = compile_code(&ucfg);
			lprintf(log_file, "Compile Procedure return value %d (%s)\n", ret, strcode(ret));
		}
		
		// use the monitor program to run
		int mem_peak = 0; double elapsed = 0;
		if (ret == OK) {
			lprintf(log_file, "%sRunning Test Case %d\n", i ? "\n" : "", i);
			ret = monitor_program(&ucfg, log_file, &elapsed, &mem_peak);
			lprintf(log_file, "Monitor Program return value %d (%s)\n", ret, strcode(ret));
		}
		
		// convert return code to print and calculate resource usages
		strcatf(ucfg.res.result, "%s\"%s\"", (i ? "," : ""), strcode(ret));
		ucfg.res.execute_time += elapsed;
		ucfg.res.memory_usage = max(ucfg.res.memory_usage, mem_peak);
		
		// close file or add delimiter depending on redirection
		if (ucfg.in != stdin) fclose(ucfg.in);
		if (ucfg.out != stdout) fclose(ucfg.out); else fprintf(stdout, "%c", DELIM_VAL);
		if (ucfg.err != stderr) fclose(ucfg.err); else fprintf(stderr, "%c", DELIM_VAL);
		
		// check if continue depending on ret and error_lv
		int cont = 1;
		switch (ucfg.error_lv) {
			default:
			case ALL_ERROR:
				cont = ret == OK;			// stop if not OK
				break;
			case TLE_CE_ONLY:
				cont = cont && ret != TLE;	// fall through
			case CE_ONLY:
				cont = cont && ret != CE && ret != ENF;
		}
		if (!cont) break;
	} while (++i < ucfg.num_cases);
	
	// print the json then close all files
	print_result_json(&ucfg);
	lclose(log_file);
	
	return 0;
}
