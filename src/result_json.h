#ifndef RESULT_JSON
#define RESULT_JSON

#include "common.h"

/* prototypes */
void print_result_json(struct user_config *ucfg);
const char *strcode(int code);

#endif /* RESULT_JSON */
